/* 
    express.js
    API creatiuon with express js\

    open source javascript runtime environment for creating server side applciations


    Node.js review

    Runtime Enviroment
        Gives the context for running a programming janguage

        JS was initially within the context of the browser, giving it access to 
            DOM 

    Limitation 
        node.js does not have direct supprot for common web development tsks such as :
            > Handling HTTP verbs (get , post delete)
            > Handling requests at different URL paths
            > Serving of static files.
        
            This means tha using node.js on its own for web development may get tedious

    Instead of writing the code for these common web development tasks yourself, save your time and efforet and use a web framework such as express.js instead.
*/

// What is express JS?
/*
    unopinionated web application framework written in JS and hosted in node.

    Web Framework 
        A set of ocomponents designed to simplify the web dev process
            Convention over configuration 
        
            Allows devs to focus of the business logic of their app.

            comes in two forms : opinionated and unopinionated 

            Opinionated Web Framework
            The framework dictates how it should be used bby the dev.
            Speeds up the startup process of app development
            enformces best practices for the framework's use case 

            Lack of flexibility could be a drawback when app's needs are not aligned with the framework's assumptions.

            Unopinionated Web Framework 
            The dev dictates how to use the framework
            Offers flexibility to create an application unbound by any use case 

            No right way of structuring an application

            abundance of options may be ooverwhelming

            Advantages of express.js
                >>Simplicity makes it easy to learn and use
                >>offers ready to use components for the most common web development needs
                >>adds easier routing and processing of HTTP methods


            
*/

// this require ("express") allows devs to load/import express package that will be used for the application 

let express = require("express");

// express() - allows devs to create an application using express

const app = express(); // this code creates an express applciation and stores it inside the "app" variable. thus, "app" is the server already.

const port = 3000;

// use functiion lets the middleware to do common services and capabilites to the applications.
    // dfl
app.use(express.json()); // lets the application to read json data 
app.use(express.urlencoded({extended: true})) // allows the app to receive data from the forms.

    // not using these will result to out req.body to return undefined

// express has methods corresponding to each http methods (get, post, put, delete, etc)
// this "/" route expects to recieve a get request at its endpoint (http://localhost:3000/)
app.get("/", (req,res) => {
    // res.send - allows sending of messages as responses to thec lient
    res.send("hello world")
})


/* 
    create a  "/hello" route that will receive a GET request and will send a message to the client, send the screenshot of your outputs in the batch google chat
*/


app.get("/hello", (req,res) => {
    // res.send - allows sending of messages as responses to thec lient
    res.send("hello from the /hello endpoint")
})

// this "/hello" route is expected to receive a post request that has json data in the body.

app.post('/hello', (req,res) => {
    // to check if the json keys in the body has values
    console.log(req.body.firstName)
    console.log(req.body.lastName)
    // sends the response one the req.body.firstName and req.body.lastName has values
    res.send(`hello there ${req.body.firstName} ${req.body.lastName}`)
})


// Activity 

app.get("/home", (req,res) => {
    res.send("Welcome to Home")
})


app.get("/users", (req,res) => {
    res.json(
        [

            {
                "_userID" : "0001",
                "fName" : "Jherson ",
                "lname" : "Dignadice",
                "email" : "j.dignadice.27@gmail.com",
                "isAdmin" : true,
                "mobileNumber" : "092310293"
            },
        
            {
                "_userID" : "0002",
                "fName" : "jhersy",
                "lname" : "Dignadice",
                "email" : "jhersy@gmail.com",
                "isAdmin" : false,
                "mobileNumber" : "09234321324"
            }
        ]
        
        
    )
})

app.delete("/delete-user", (req,res) => {
    console.log("Delete")
})


// stretch goal

app.post('/sign_up', (req,res) => {
    res.send(`${req.body.username} ${req.body.password}`)
})
 



app.listen(port, () => console.log(`Server is running at port ${port}`))


/* 

Activity:
1. Create a GET route that will access the "/home" route that will print out a simple message.
2. Process a GET request at the "/home" route using postman.
3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
4. Process a GET request at the "/users" route using postman.
5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
6. Process a DELETE request at the "/delete-user" route using postman.
7. Export the Postman collection and save it inside the root folder of our application.
8. Create a git repository named S29.
9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
10. Add the link in Boodle.

Stretch goal:
-create a users variable that will accept an array of objects
-create /signup route that will accept post method.
-once the username and the password in the body is filled with information, push the object into the users array and print the message of "username has registered successfully", otherwise, send "please input both username and password"

*/


